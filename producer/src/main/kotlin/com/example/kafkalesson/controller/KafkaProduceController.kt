package com.example.kafkalesson.controller

import org.springframework.kafka.core.KafkaTemplate
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.time.LocalDateTime
import java.util.*

@RestController
@RequestMapping("/api/kafka")
class KafkaProduceController(
    private val kafkaTemplate: KafkaTemplate<String, String>
) {

    @GetMapping("/{count}")
    fun produceRecords(@PathVariable count: Int) {
        for (i in 0..count) {
            kafkaTemplate.send("offset-example", getOneMessage())
        }
    }

    @GetMapping("/one")
    fun produceRecord() {
        kafkaTemplate.send("offset-example", getOneMessage())
    }

    private fun getOneMessage(): String {
        val message = "date: ${LocalDateTime.now()} uuid: ${UUID.randomUUID()}"
        println(message)
        return message
    }

}