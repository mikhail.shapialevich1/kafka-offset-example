package com.example.kafkalesson.enums

enum class OffsetType {
    ABSOLUTE, RELATIVE
}