package com.example.kafkalesson.controllers

import com.example.kafkalesson.config.KafkaConsumerAbsoluteConfig
import com.example.kafkalesson.config.KafkaConsumerRelativeOffsetConfig
import com.example.kafkalesson.enums.OffsetType
import org.springframework.kafka.config.KafkaListenerEndpointRegistry
import org.springframework.kafka.listener.MessageListenerContainer
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.util.*

@RestController
@RequestMapping("/api/consumer")
class ConsumerController(
    private val kafkaListenerEndpointRegistry: KafkaListenerEndpointRegistry,
    private val kafkaConsumerAbsoluteConfig: KafkaConsumerAbsoluteConfig,
    private val consumerRelative: KafkaConsumerRelativeOffsetConfig
) {

    @PostMapping("/listener_id")
    fun getConsumerIds(@RequestParam topicName: String): List<String?> {
        return kafkaListenerEndpointRegistry.allListenerContainers
            .filter { con ->
                con.containerProperties.topics?.contains(topicName)
                    ?: throw RuntimeException(String.format("Consumer with topic %s is not found", topicName))
            }
            .map { con -> con.listenerId }
    }

    @PostMapping("/activate")
    fun activateConsumer(@RequestParam consumerId: String) {
        val listenerContainer: MessageListenerContainer? =
            kafkaListenerEndpointRegistry.getListenerContainer(consumerId)
        if (Objects.isNull(listenerContainer)) {
            throw RuntimeException(String.format("Consumer with id %s is not found", consumerId))
        } else if (listenerContainer != null) {
            if (listenerContainer.isRunning) {
                throw RuntimeException(String.format("Consumer with id %s is already running", consumerId))
            } else {
                listenerContainer.start()
            }
        }
    }

    @PostMapping("/deactivate")
    fun deactivateConsumer(@RequestParam consumerId: String) {
        val listenerContainer: MessageListenerContainer? =
            kafkaListenerEndpointRegistry.getListenerContainer(consumerId)
        if (Objects.isNull(listenerContainer)) {
            throw RuntimeException(String.format("Consumer with id %s is not found", consumerId))
        } else if (listenerContainer != null) {
            if (!listenerContainer.isRunning) {
                throw RuntimeException(String.format("Consumer with id %s is already stopped", consumerId))
            } else {
                listenerContainer.stop()
            }
        }
    }

    @PostMapping("/offset/absolute")
    fun setAbsoluteOffset(@RequestParam offset: Long) {
        kafkaConsumerAbsoluteConfig.offset = offset
    }

    @PostMapping("/offset/relative")
    fun setRelativeOffset(@RequestParam offset: Long) {
        consumerRelative.offset = offset
    }

    @PostMapping("/reload_with_offset")
    fun reloadWithOffset(
        @RequestParam consumerId: String,
        @RequestParam offset: Long,
        @RequestParam offsetType: OffsetType
    ) {
        val listenerContainer: MessageListenerContainer? =
            kafkaListenerEndpointRegistry.getListenerContainer(consumerId)
        if (Objects.isNull(listenerContainer)) {
            throw RuntimeException(String.format("Consumer with id %s is not found", consumerId))
        } else if (listenerContainer != null) {
            if (listenerContainer.isRunning) {
                listenerContainer.stop()
            }
            when (offsetType) {
                OffsetType.ABSOLUTE -> setAbsoluteOffset(offset)
                OffsetType.RELATIVE -> setRelativeOffset(offset)
            }
            listenerContainer.start()
        }
    }
}
