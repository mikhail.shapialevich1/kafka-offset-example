package com.example.kafkalesson

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class KafkaLessonApplication

fun main(args: Array<String>) {
    runApplication<KafkaLessonApplication>(*args)
}
